package kg.attractor.lesson50.model;

import kg.attractor.lesson50.util.Generator;
import lombok.Builder;
import lombok.Data;
import lombok.Generated;
import org.apache.tomcat.jni.Local;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

@Document
@Data
@Builder
public class Comment {
    @Id
    private String id;
    private String text;
    private LocalDateTime date;
    private int likes;
    @DBRef
    private User user;
    @DBRef
    private Post post;

    private static String generateId(LocalDateTime date) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        Random r = new Random();
        return date.now().format(dtf) + "-" + r.nextInt(1_000_000);
    }

    public static Comment addComment(User user, Post post) {
        return builder()
                .id(generateId(LocalDateTime.now()))
                .text(Generator.makeDescription())
                .date(LocalDateTime.now())
                .user(user)
                .post(post)
                .build();
    }


}