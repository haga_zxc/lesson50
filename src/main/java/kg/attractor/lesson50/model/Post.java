package kg.attractor.lesson50.model;

import kg.attractor.lesson50.util.Generator;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

@Document
@Data
@Builder
public class Post {
    @Id
    private String id;
    private String description;
    private LocalDateTime date;
    private int likes;
    private String image;
    @DBRef
    private User user;

    @DBRef
    @Builder.Default
    private PostImage postImage;

    private static String generateId(LocalDateTime date) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        Random r = new Random();
        return date.now().format(dtf) + "-" + r.nextInt(1_000_000);
    }

    public static Post addPost(User user) {
        return builder()
                .id(generateId(LocalDateTime.now()))
                .user(user)
                .description(Generator.makeDescription())
                .date(LocalDateTime.now())
                .image("http://localhost:4949/user/image/java.jpg")
                .build();
    }

}
