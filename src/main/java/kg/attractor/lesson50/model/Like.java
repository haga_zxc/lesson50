package kg.attractor.lesson50.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.DBRef;


import java.time.LocalDate;

    @Data
    public class Like {
        private LocalDate date;
        @DBRef
        private User user;
        @DBRef
        private Post post;

}
