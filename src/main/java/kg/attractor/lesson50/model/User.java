package kg.attractor.lesson50.model;

import kg.attractor.lesson50.util.Generator;
import kg.attractor.lesson50.util.SecurityConfig;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.List;
import java.util.Random;

@Document
@Data
@Builder
public class User implements UserDetails {
    @Builder.Default
    @Id
    private String id = generateId(LocalDateTime.now());
    private String name;
    private String email;
    private String login;
    private String password;
    private int postCol;
    @DBRef
    private List<User> subscribers;
    @DBRef
    private List<User> subscriptions;


    private static String generateId(LocalDateTime date) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        Random r = new Random();
        return date.now().format(dtf) + "-" + r.nextInt(1_000_000);
    }

    public static User random() {
        return builder()
                .email(Generator.makeEmail())
                .name(Generator.makeName())
                .password(SecurityConfig.encoder().encode(Generator.makePassword()))
                .build();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority("FULL"));
    }

    @Override
    public String getUsername() {
        return getLogin();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setPassword(String password){
        this.password = SecurityConfig.encoder().encode(password);
    }


}
