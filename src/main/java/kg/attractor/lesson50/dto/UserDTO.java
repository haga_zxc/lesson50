package kg.attractor.lesson50.dto;

import kg.attractor.lesson50.model.User;
import lombok.*;

import java.util.List;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class UserDTO {

    public static UserDTO from(User user) {
        return builder()
                .id(user.getId())
                .name(user.getName())
                .email(user.getEmail())
                .password(user.getPassword())
                .postCol(user.getPostCol())
                .login(user.getLogin())
                .subscribers(user.getSubscribers())
                .subscriptions(user.getSubscriptions())
                .build();
    }

    private String id;
    private String name;
    private String email;
    private String password;
    private String login;
    private int postCol;
    private List<User>subscribers;
    private List<User>subscriptions;

}
