package kg.attractor.lesson50.dto;

import kg.attractor.lesson50.model.Post;
import kg.attractor.lesson50.model.PostImage;
import lombok.*;
import org.bson.types.Binary;

import java.time.LocalDateTime;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class PostDTO {

    public static PostDTO from(Post post) {
        var postPhotoId = post.getPostImage() == null
                ? "-no-image-id"
                : post.getPostImage().getId();

        return builder()
                .id(post.getId())
                .imageId(postPhotoId)
                .description(post.getDescription())
                .date(post.getDate())
                .userId(post.getUser().getId())
                .likes(post.getLikes())
                .image(post.getImage())
                .postImage(post.getPostImage())
                .build();
    }

    private String id;
    private String imageId;
    private String description;
    private LocalDateTime date;
    private String userId;
    private int likes;
    private String image;
    private PostImage postImage;


}
