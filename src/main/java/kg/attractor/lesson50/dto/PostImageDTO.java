package kg.attractor.lesson50.dto;

import lombok.*;

    @Data
    @Builder
    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    @NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
    public class PostImageDTO {
        private String imageId;
    }
