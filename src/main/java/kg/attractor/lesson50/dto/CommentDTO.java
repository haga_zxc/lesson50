package kg.attractor.lesson50.dto;

import kg.attractor.lesson50.model.Comment;
import kg.attractor.lesson50.model.Post;
import lombok.*;

import java.time.LocalDateTime;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class CommentDTO {

    public static CommentDTO from(Comment comment) {
        return builder()
                .id(comment.getId())
                .text(comment.getText())
                .date(comment.getDate())
                .userId(comment.getUser().getId())
                .likes(comment.getLikes())
                .postId(comment.getPost().getId())
                .name(comment.getUser().getName())
                .build();
    }

    private String id;
    private String text;
    private LocalDateTime date;
    private String name;
    private String userId;
    private String postId;
    private int likes;


}
