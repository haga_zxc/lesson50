package kg.attractor.lesson50.controller;


import kg.attractor.lesson50.anotation.ApiPageable;
import kg.attractor.lesson50.dto.PostDTO;
import kg.attractor.lesson50.dto.UserDTO;
import kg.attractor.lesson50.model.Comment;
import kg.attractor.lesson50.model.Post;
import kg.attractor.lesson50.model.User;
import kg.attractor.lesson50.repository.CommentRepository;
import kg.attractor.lesson50.repository.PostRepository;
import kg.attractor.lesson50.repository.UserRepository;
import kg.attractor.lesson50.service.PostImageService;
import kg.attractor.lesson50.service.PostService;
import kg.attractor.lesson50.service.UserService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import java.io.*;


@Controller
public class LoginController {
    @GetMapping("/registration")
    public String registration(){
        return "registration";
    }
    @GetMapping("/login")
    public String login(){
        return "login";
    }
}
