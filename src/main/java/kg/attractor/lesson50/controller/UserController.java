package kg.attractor.lesson50.controller;


import kg.attractor.lesson50.anotation.ApiPageable;
import kg.attractor.lesson50.dto.PostDTO;
import kg.attractor.lesson50.dto.UserDTO;
import kg.attractor.lesson50.model.Comment;
import kg.attractor.lesson50.model.Post;
import kg.attractor.lesson50.model.User;
import kg.attractor.lesson50.repository.CommentRepository;
import kg.attractor.lesson50.repository.PostRepository;
import kg.attractor.lesson50.repository.UserRepository;
import kg.attractor.lesson50.service.PostImageService;
import kg.attractor.lesson50.service.PostService;
import kg.attractor.lesson50.service.UserService;
import kg.attractor.lesson50.util.SecurityConfig;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.support.SimpleTriggerContext;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;


import java.io.*;


@Controller
@RequestMapping("/user")
public class UserController {
   private final UserRepository userRepository;
   private final UserService userService;
    private final PostService postService;
    private final PostImageService postImageService;
    private final PostRepository postRepository;
    private final CommentRepository commentRepository;

    public UserController(UserService userService,UserRepository userRepository, PostService postService, PostImageService postImageService, PostRepository postRepository, CommentRepository commentRepository) {
        this.userRepository = userRepository;
        this.userService = userService;
        this.postService = postService;
        this.postImageService = postImageService;
        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
    }

    @ApiPageable
    @GetMapping
    public Slice<UserDTO> findUsers(@ApiIgnore Pageable pageable) {
        return userService.findUsers(pageable);
    }

    @GetMapping("/byName/{name}")            //Поиск пользователей по имени
    public UserDTO getUserByName(@PathVariable("name") String name) {
        return userService.findOneByName(name);
    }

    @GetMapping("/byEmail/{email}")            //Поиск пользователей по email
    public UserDTO getUserByEmail(@PathVariable("email") String email) {
        return userService.findOneByEmail(email);
    }

    @GetMapping("/checkEmail/{email}")
    public boolean checkUser(@PathVariable("email") String email) {
        return userService.emailCheck(email);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserDTO addNewUser(@RequestBody UserDTO userData) {
        return userService.addUser(userData);
    }

    @PostMapping(path ="/add_post" , consumes = MediaType.APPLICATION_JSON_VALUE)
    public PostDTO addPost(@RequestBody PostDTO postData) {
        return postService.addPost(postData);
    }

    @GetMapping("/posts")
    public String showPost(){
        return "index";

    }
    @PostMapping(path = "/saveImage", produces = MediaType.APPLICATION_JSON_VALUE)
    public String saveImage(@RequestParam("image") MultipartFile file,
                            Authentication authentication) throws IOException {
             File image = new File("D:/images/"+file.getOriginalFilename());
             FileOutputStream os = new FileOutputStream(image);
             os.write(file.getBytes());
             os.close();
             var user = (User)authentication.getPrincipal();
             var post = Post.addPost(user);
             post.setImage("http://localhost:4949/user/image/" + image.getName());
             postRepository.save(post);

            return "redirct:/user/posts";
    }
    @PostMapping(path = "/saveComment",produces = MediaType.APPLICATION_JSON_VALUE)
    public String saveComment(@RequestParam("comment") String comment,
                              @RequestParam("postId") String id,
                              Authentication authentication){
        var user = (User)authentication.getPrincipal();
        var newPost = Post.addPost(user);
        newPost.setId("123");
        postRepository.save(newPost);
        var newComment = Comment.addComment(user, postRepository.findById(id).get());
        newComment.setText(comment);
        commentRepository.save(newComment);

        return "index";
    }

    @GetMapping("/image/{name}")
    @ResponseBody
    public ResponseEntity<byte[]> getImage(@PathVariable("name")String name){
        String path = "D:\\images";
        try {
            InputStream is = new FileInputStream(new File(path) + "/" + name);
            return ResponseEntity
                    .ok()
                    .contentType(name.toLowerCase().contains(".jpg") ? MediaType.IMAGE_JPEG : MediaType.IMAGE_PNG)
                    .body(StreamUtils.copyToByteArray(is));
        }catch (Exception e){
            InputStream is = null;
        try {
            is = new FileInputStream(new File(path) + "/" + name);
            return ResponseEntity
                    .ok()
                    .contentType(name.toLowerCase().contains(".jpg") ? MediaType.IMAGE_JPEG : MediaType.IMAGE_PNG)
                    .body(StreamUtils.copyToByteArray(is));
        }catch (IOException ex){
            ex.printStackTrace();
        }
        e.printStackTrace();
        }
        return null;
    }

    @PostMapping("/registration")
    public User createUser(@RequestParam("email")String email,@RequestParam("name")String name,
                           @RequestParam("login")String login,@RequestParam("password")String password){
            var user = User.random();
            user.setEmail(email);
            user.setPassword(password);
            user.setLogin(login);
            user.setName(name);
            userRepository.save(user);
            return user;
    }



}
