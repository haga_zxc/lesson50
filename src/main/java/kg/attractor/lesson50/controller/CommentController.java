package kg.attractor.lesson50.controller;

import kg.attractor.lesson50.anotation.ApiPageable;
import kg.attractor.lesson50.dto.CommentDTO;
import kg.attractor.lesson50.service.CommentService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/comments")
public class CommentController {
    CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @ApiPageable
    @GetMapping
    public Slice<CommentDTO> findAllComments(@ApiIgnore Pageable pageable) {
        return commentService.findComments(pageable);
    }

//    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
//    public CommentDTO addNewComment(@RequestBody CommentDTO commentData) {
//        return commentService.addComment(commentData);
//    }
}
