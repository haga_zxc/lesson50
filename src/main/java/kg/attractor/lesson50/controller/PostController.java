package kg.attractor.lesson50.controller;

import kg.attractor.lesson50.anotation.ApiPageable;
import kg.attractor.lesson50.dto.PostDTO;
import kg.attractor.lesson50.model.Post;
import kg.attractor.lesson50.service.PostImageService;
import kg.attractor.lesson50.service.PostService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/posts")
public class PostController {

    PostService postService;
    PostImageService postImageService;

    public PostController(PostService postService) {
        this.postService = postService;
    }

    @ApiPageable
    @GetMapping("/by_user_id/{user_id}")                    //Выборка публикаций других пользователей.
    public Slice<PostDTO> getAllUsersPost(@ApiIgnore Pageable pageable, @PathVariable("user_id") String user_id) {
        return postService.findAllUserPost(pageable, user_id);
    }

    @ApiPageable
    @GetMapping
    public Slice<PostDTO> findPosts(@ApiIgnore Pageable pageable) {
        return postService.findPosts(pageable);
    }
}
