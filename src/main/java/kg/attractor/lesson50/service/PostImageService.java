package kg.attractor.lesson50.service;

import kg.attractor.lesson50.dto.PostImageDTO;
import kg.attractor.lesson50.exception.ResourceNotFoundException;
import kg.attractor.lesson50.model.PostImage;
import kg.attractor.lesson50.repository.PostImageRepository;
import org.bson.types.Binary;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class PostImageService {
    private final PostImageRepository postImageRepository;

    public PostImageService(PostImageRepository postImageRepository) {
        this.postImageRepository = postImageRepository;
    }

    public PostImage addImage(MultipartFile file) {
        byte[] data = new byte[0];
        try {
            data = file.getBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (data.length == 0) {
            // TODO return no content or something or throw exception
            //  which will be processed on controller layer
        }

        Binary bData = new Binary(data);
        PostImage postImage = PostImage.builder().postData(bData).build();

        postImageRepository.save(postImage);

           return postImage;

    }

    public Resource getById(String imageId) {
        PostImage postImage = postImageRepository.findById(imageId)
                .orElseThrow(() -> new ResourceNotFoundException("Post Image with " + imageId + " doesn't exists!"));
        return new ByteArrayResource(postImage.getPostData().getData());
    }
}
