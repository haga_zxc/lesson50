package kg.attractor.lesson50.service;

import kg.attractor.lesson50.dto.PostDTO;
import kg.attractor.lesson50.dto.UserDTO;
import kg.attractor.lesson50.exception.ResourceNotFoundException;
import kg.attractor.lesson50.model.*;
import kg.attractor.lesson50.repository.CommentRepository;
import kg.attractor.lesson50.repository.LikeRepository;
import kg.attractor.lesson50.repository.PostRepository;
import kg.attractor.lesson50.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
    private UserRepository ur;
    private PostRepository pr;
    private LikeRepository lr;

    @Autowired
    public UserService(UserRepository ur, PostRepository pr, LikeRepository lr) {
        this.ur = ur;
        this.pr = pr;
        this.lr = lr;
    }

    public List<Post> getUsersPublications(String id) {
        return pr.findAllByUserId(id);
    }

    public List<Post> getPostFormSubUsers(String id) {          //выборка всех постов пользователей на которыъ ты подписан
        List<Post> result = new ArrayList<>();
        List<User> subscriptions = ur.findUserById(id).get(0).getSubscriptions();
        for (User user : subscriptions) {
            result.addAll(getUsersPublications(user.getId()));
        }

        return result;
    }

    public boolean checkLikeForPublication(String user_id, String post_id) {          //проверка на лайк
        boolean check = false;
        if (lr.existsByUser_Id(user_id)) {
            for (Like like : lr.findAllByUser_Id(user_id)) {
                if (like.getPost().getId().equals(post_id)) {
                    check = true;
                }
                break;
            }
        }
        return check;
    }

    public Slice<UserDTO> findUsers(Pageable pageable) {
        var slice = ur.findAll(pageable);
        return slice.map(UserDTO::from);
    }

    public UserDTO findOneByName(String userName) {
        var user = ur.findByName(userName)
                .orElseThrow(() -> new ResourceNotFoundException("Can't find User with the name: " + userName));
        return UserDTO.from(user);
    }

    public UserDTO findOneByEmail(String email) {
        var user = ur.findByEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException("Can't find User with the email: " + email));
        return UserDTO.from(user);
    }

    public boolean emailCheck(String email) {
        var check = ur.existsByEmail(email);
        return check;
    }

    public UserDTO addUser(UserDTO userDTO) {
        var user = User.builder()
                .id(userDTO.getId())
                .name(userDTO.getName())
                .email(userDTO.getEmail())
                .password(userDTO.getPassword())
                .build();
        ur.save(user);

        // TODO recalculate rating after save. Update movie document

        return UserDTO.from(user);
    }



}

