package kg.attractor.lesson50.service;

import kg.attractor.lesson50.dto.PostDTO;
import kg.attractor.lesson50.exception.ResourceNotFoundException;
import kg.attractor.lesson50.model.Post;
import kg.attractor.lesson50.repository.PostImageRepository;
import kg.attractor.lesson50.repository.PostRepository;
import kg.attractor.lesson50.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Map;

@Service
public class PostService {
    final
    PostRepository postRepository;
    final
    UserRepository userRepository;
    final
    PostImageRepository postImageRepository;

    @Autowired
    public PostService(PostImageRepository postImageRepository, PostRepository postRepository, UserRepository userRepository) {
        this.postImageRepository = postImageRepository;
        this.postRepository = postRepository;
        this.userRepository = userRepository;
    }

    public Slice<PostDTO> findAllUserPost(Pageable pageable, String userId) {
        var slice = postRepository.findAllByUser_Id(userId, pageable);
        return slice.map(PostDTO::from);
    }

    public PostDTO addPost(PostDTO postData) {
        var postImage = postImageRepository.findById(postData.getImageId())
                .orElseThrow(() -> new ResourceNotFoundException("Post Image with " + postData.getImageId() + " doesn't exists!"));
//        //TODO one user can have only one review per movie. add check
        var user = userRepository.findById(postData.getUserId()).get();
        var post = Post.builder()
                .id(postData.getId())
                .postImage(postImage)
                .user(user)
                .date(postData.getDate())
                .description(postData.getDescription())
                .build();
        postRepository.save(post);

        // TODO recalculate rating after save. Update movie document

        return PostDTO.from(post);
    }

    public Slice<PostDTO> findPosts(Pageable pageable) {
        var slice = postRepository.findAll(pageable);
        return slice.map(PostDTO::from);
    }
}
