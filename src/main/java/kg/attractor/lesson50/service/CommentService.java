package kg.attractor.lesson50.service;

import kg.attractor.lesson50.dto.CommentDTO;
import kg.attractor.lesson50.dto.UserDTO;
import kg.attractor.lesson50.model.Comment;
import kg.attractor.lesson50.model.User;
import kg.attractor.lesson50.repository.CommentRepository;
import kg.attractor.lesson50.repository.PostRepository;
import kg.attractor.lesson50.repository.UserRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

@Service
public class CommentService {
    private final CommentRepository commentRepository;
    private final UserRepository userRepository;
    private final PostRepository postRepository;


    public CommentService(CommentRepository commentRepository, UserRepository userRepository, PostRepository postRepository) {

        this.commentRepository = commentRepository;
        this.userRepository = userRepository;
        this.postRepository = postRepository;
    }

    public Slice<CommentDTO> findComments(Pageable pageable) {
        var slice = commentRepository.findAll(pageable);
        return slice.map(CommentDTO::from);
    }

//    public CommentDTO addComment(CommentDTO commentData) {
//        var user = userRepository.findById(commentData.getUserId()).get();
//        var post = postRepository.findById(commentData.getPostId()).get();
//        var comment = Comment.builder()
//                .text(commentData.getText())
//                .date(commentData.getDate())
//                .user(user)
//                .post(post)
//                .build();
//        commentRepository.save(comment);
//
//        // TODO recalculate rating after save. Update movie document
//
//        return CommentDTO.from(comment);
//    }
}
