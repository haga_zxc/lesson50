package kg.attractor.lesson50.util;

import kg.attractor.lesson50.model.*;
import kg.attractor.lesson50.repository.CommentRepository;
import kg.attractor.lesson50.repository.PostImageRepository;
import kg.attractor.lesson50.repository.PostRepository;
import kg.attractor.lesson50.repository.UserRepository;
import kg.attractor.lesson50.service.PostImageService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Configuration
public class DataBaseLoader {
    private static final Random r = new Random();


    @Bean
    CommandLineRunner initDatabase(UserRepository userRepository, PostRepository postRepository,
                                   CommentRepository commentRepository, PostImageRepository postImageRepository) {
        return (args) -> {
            userRepository.deleteAll();
            postRepository.deleteAll();
            commentRepository.deleteAll();
            postImageRepository.deleteAll();


            var demoUser = User.random();
            demoUser.setEmail("123@gmail.com");
            demoUser.setPassword("qwerty");
            demoUser.setLogin("Haga");
            userRepository.save(demoUser);

            List<User> users = Stream.generate(User::random)
                    .limit(10)
                    .collect(toList());
            userRepository.saveAll(users);

//            List<Post> posts = Stream.generate(() -> Post.addPost(users.get(r.nextInt(users.size()))))
//                    .limit(0)
//                    .collect(toList());
//            postRepository.saveAll(posts);

//            List<Comment> comments = Stream.generate(() -> Comment.addComment(users.get(r.nextInt(users.size())), posts.get(r.nextInt(posts.size()))))
//                    .limit(10)
//                    .collect(toList());
//            commentRepository.saveAll(comments);
//            System.out.println("done");
        };
    }
}
