package kg.attractor.lesson50.repository;

import kg.attractor.lesson50.model.Comment;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface CommentRepository extends PagingAndSortingRepository<Comment, String> {

}
