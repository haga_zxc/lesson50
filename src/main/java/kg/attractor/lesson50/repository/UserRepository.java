package kg.attractor.lesson50.repository;


import kg.attractor.lesson50.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;


public interface UserRepository extends PagingAndSortingRepository<User, String> {
    Optional<User> findByName(String name);
    Optional<User> findByEmail(String email);
    Optional<User> findByLogin(String login);
    boolean existsByEmail(String email);
    List<User> findUserById(String id);
}
