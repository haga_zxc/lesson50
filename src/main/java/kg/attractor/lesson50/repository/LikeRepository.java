package kg.attractor.lesson50.repository;

import kg.attractor.lesson50.model.Like;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LikeRepository extends CrudRepository<Like, String> {
    boolean existsByUser_Id(String id);

    List<Like> findAllByUser_Id(String id);
}
