package kg.attractor.lesson50.repository;

import kg.attractor.lesson50.model.PostImage;
import org.springframework.data.repository.CrudRepository;

public interface PostImageRepository extends CrudRepository<PostImage, String> {
}
