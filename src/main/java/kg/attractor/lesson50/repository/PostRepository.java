package kg.attractor.lesson50.repository;

import kg.attractor.lesson50.model.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface PostRepository extends PagingAndSortingRepository<Post, String> {
    @Query("{'id': {'$ne': '?0'}}")
    Page<Post> findAllByUser_Id(String id, Pageable pageable);   //Выборка публикаций других пользователей.

    List<Post> findAllByUserId(String id);
}
