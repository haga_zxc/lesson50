'use strict';
const baseUrl = 'http://localhost:4949';

    const registrationForm = document.getElementById('registration-form');
    registrationForm.addEventListener('submit', onRegisterHandler);

    function onRegisterHandler(e) {
        e.preventDefault();
        const form = e.target;
        const data = new FormData(form);
        createUser(data).catch(console.error);
    }
async function createUser(userJSON) {
    const settings = {
        method: 'POST',
        body: userJSON
    };

    const response = await fetch('http://localhost:4949/user/registration', settings);
    const responseData = await response.json();
    console.log(responseData);
    window.location.href = 'http://localhost:4949/login';
}