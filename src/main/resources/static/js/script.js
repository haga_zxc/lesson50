'use strict';

//SPLASH SCREEN //SPLASH SCREEN //SPLASH SCREEN //SPLASH SCREEN //SPLASH SCREEN //SPLASH SCREEN
function showSplashScreen() {
    let x = document.getElementById("page-splash");
    x.style.visibility = "visible";
    let y = document.getElementById('body');
    y.classList.add("no-scroll");

}

function hideSplashScreen() {
    let x = document.getElementById("page-splash");
    let y = document.getElementById('body');
    x.style.visibility = "hidden"
    y.classList.remove("no-scroll");
}


function createCommentElement(x) {
    let div = document.createElement('div');
    const html = `
        <a href=# class=muted>${x.name}</a>
         <p>${x.text}</p>;
        `;
    div.innerHTML+=html;
    div.classList.add("py-3", "pl-3");
    div.id = "comment" + x.id
    return div;
}

function createPostElement(y) {
    let div = document.createElement('div');
    const html = `
    <div >
         <img id="image${y.id}" class="d-block w-100 image-1" src="${y.image} " alt="Post image">
    </div>
  <div class="px-4 py-3" id="post${y.id}">
    <div class="d-flex justify-content-around">
       <span class="h1 mx-2 muted">
          <i class="far fa-heart"  id="like${y.id}"></i>
        </span>
        <span class="h1 mx-2 muted">
          <i class="far fa-comment" id="comment${y.id}"></i>
        </span>
        <span class="mx-auto"></span>
        <span class="h1 mx-2 muted" id="save${y.id}">
          <i class="far fa-bookmark"></i>
        </span>
    </div>
  <hr>
  <div>
    <p>${y.description}</p>
    </div>
  <hr>
  <div id = "commentArea${y.id}" class="comment-container">
  </div>
   <div id="commentForm${y.id}">
          <h2>Add Comment</h2>
          <form enctype="multipart/form-data" class="commentForm" method="post" action="/user/saveComment" id="commentForm${y.id}">
              <input type="hidden" id="userId" value="45">
              <input type="hidden" id="postId" value="${y.id}">
              <textarea cols="50"  rows="3" id="newComment${y.id} class="text-area"></textarea>
              <button id="commentButton" >Confirm</button>
              <button onclick="f123()" >Add Comment</button>
          </form>
   </div>
  </div>
  `;
    div.innerHTML += html;
    div.classList.add("card", "my-3");
    return div;
}


function addPost(x) {
    document.getElementsByClassName('posts-container')[0].append(x);
}
    $(document).on('click', '.fa-heart', function (e) {
        e.preventDefault();
        let thisLike = e.currentTarget;
        thisLike.classList.contains('text-danger')
            ? thisLike.classList.remove('text-danger', 'fas')
            : thisLike.classList.add('text-danger', 'fas');
    });


    $(document).on('click', '.fa-bookmark', function (e) {
        e.preventDefault();
        let thisLike = e.currentTarget;
        thisLike.classList.contains('fas')
            ? thisLike.classList.remove('fas')
            : thisLike.classList.add('fas');
    });

$(document).on('dblclick', '.image-1', function (e) {
    let thisImage = e.currentTarget;
    let imageId = thisImage.id.substr(5 - thisImage.id.length);
    let thisLike = document.getElementById("like"+imageId);
    thisLike.classList.contains('text-danger')
        ? thisLike.classList.remove('text-danger', 'fas')
        : thisLike.classList.add('text-danger', 'fas');
});

const url = '/user/saveImage';

document.addEventListener('DOMContentLoaded', init);

function init() {
    document.getElementById('btnSubmit').addEventListener('click', upload);
}

function upload(ev) {
    ev.preventDefault();
    let h = new Headers();
    h.append('Accept', 'application/json');
    let fd = new FormData();
    let myFile = document.getElementById('image').files[0];
    fd.append('image', myFile, myFile.name);
    let req = new Request(url, {
        method: 'POST',
        headers: h,
        body: fd
    });

    fetch(req)
        .catch((err) => {
            console.log('ERROR:', err.message);
        });
}

$(document).on('click', '.fa-comment', function (e) {
    let thisComment = e.currentTarget;
    let commentId = thisComment.id.substr(7 - thisComment.id.length);
    let comment = document.getElementById("commentForm"+commentId);
    if (comment.style.visibility === "hidden") {
        comment.style.visibility = "visible";
    } else {
        comment.style.visibility = "hidden";
    }
});

async function getPosts() {
    const response = await fetch('http://localhost:4949/posts');
    if (response.ok) { // если HTTP-статус в диапазоне 200-299
        // получаем тело ответа (см. про этот метод ниже)
        let json = await response.json();
        console.log(json);
        return json;

    } else {
        alert("Ошибка HTTP: " + response.status);
    }
}

async function f() {
    let posts = await getPosts();
    posts.content.forEach(elem =>{
        !document.getElementById(`post${elem.id}`)&&addPost(createPostElement(elem));
    })
}

document.addEventListener('DOMContentLoaded', up);

function up() {
    document.getElementById('commentButton').addEventListener('click', uploadComment);
}
let commentId = "";
$(document).on('click', '.commentForm', function (e) {
    let thisComment = e.currentTarget;
    commentId = thisComment.id.substr(11 - thisComment.id.length);
});

const url1 = "/user/saveComment"
function uploadComment(e) {
    e.preventDefault();
    let h = new Headers();
    h.append('Accept', 'application/json');
    let fd = new FormData();
    let text = document.getElementById("newComment"+commentId).value;
    let id = document.getElementById("postId" + commentId).value;
    fd.append('comment', text);
    fd.append('postId', id);
    let req = new Request(url1, {
        method: 'POST',
        headers: h,
        body: fd
    });
    fetch(req)
        .catch((err) => {
            console.log('ERROR:', err.message);
        });
}
async function getComments() {
    const response = await fetch('http://localhost:4949/comments');
    if (response.ok) { // если HTTP-статус в диапазоне 200-299
        // получаем тело ответа (см. про этот метод ниже)
        let json = await response.json();
        console.log(json);
        return json;
    } else {
        alert("Ошибка HTTP: " + response.status);
    }
}

async function f123() {
    let comments = await getComments();
   for(let i = 0; i<comments.content.length;i++){
       if(comments.content[i].postId===commentId){
           if(document.getElementById("comment"+comments.content[i].id)===null) {
               let x = document.getElementById("commentArea" + commentId);
               x.append(createCommentElement(comments.content[i]));
           }else{
               return console.error();
           }
       }else{
           return console.error();
       }
   }
}
function onLoad() {
    if(localStorage.length === 0){
        showSplashScreen();
    }else{
        hideSplashScreen();
    }
}
